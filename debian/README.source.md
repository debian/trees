### General maintenance

This package is maintained in Git at 
https://salsa.debian.org/debian/trees.git .

### Importing a new upstream release

I use a cryptographically signed upstream tarball as basis for the Debian trees
package.

The upstream tarball and it's detached signature can be found at 
https://0xacab.org/riseuplabs/trees/wikis/home .

I will try to convince the maintainers of the upstream repo to use signed Git
tags, when uscan is able to do signature validation. Until then I use the signed
tarball linked from the wiki page.

To import a new upstream release into the packaging repository, do the
following:

1. Ensure you have this Git repository on your machine:

    ```
       git clone https://salsa.debian.org/debian/trees.git
    ```

2. Update the upstream remote and import it to master via:

    ```
       gbp import-orig --uscan
    ```

3. Build the package

    ```
       gbp buildpackage
    ```

    If you encounter an lintian error such as:

    ```
       orig-tarball-missing-upstream-signature trees_2.1.0.orig.tar.gz
    ```

    You need to copy the signature "trees-2.1.0.tar.gz.asc" to your build
    area and set rename it to "trees_2.1.0.orig.tar.gz.asc" . That's not 
    automated via gbp buildpackage yet.

4. To be continued, with the next release of upstream
